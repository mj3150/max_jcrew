#!/bin/bash

EAR_FILE='contact.ear'

if [ EAR_FILE != 'contact.ear' ]
then
	wget -O ${EAR_FILE} "http://nyc-wdevtool-01:8080/jenkins/job/integration-steel-appc/lastSuccessfulBuild/artifact/contact/contact-entapp/target/contact-latest.ear"
mv ${EAR_FILE} /opt/software/bm10/jcrew1/appserver/weblogic/entapps/
fi
