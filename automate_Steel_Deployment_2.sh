#!/bin/bash

EAR_FILE='jcrew.ear'
EAR_FILE_2='madewell.ear'
EAR_FILE_3='factory.ear'
EAR_FILE_4='contact.ear'
wget -O ${EAR_FILE}  "http://nyc-wdevtool-01:8080/jenkins/job/integration-steel-appc/lastSuccessfulBuild/artifact/jcrew/jcrew-entapp/target/jcrew-latest.ear"
wget -O ${EAR_FILE_2} "http://nyc-wdevtool-01:8080/jenkins/job/integration-steel-appc/lastSuccessfulBuild/artifact/madewell/madewell-entapp/target/madewell-latest.ear"
wget -O ${EAR_FILE_3} "http://nyc-wdevtool-01:8080/jenkins/job/integration-steel-appc/lastSuccessfulBuild/artifact/factory/factory-entapp/target/factory-latest.ear"
wget -O ${EAR_FILE_4} "http://nyc-wdevtool-01:8080/jenkins/job/integration-steel-appc/lastSuccessfulBuild/artifact/contact/contact-entapp/target/contact-latest.ear" 
mv ${EAR_FILE} /opt/software/bm10/jcrew1/appserver/weblogic/entapps/
mv ${EAR_FILE_2} /opt/software/bm10/jcrew1/appserver/weblogic/entapps/
mv ${EAR_FILE_3} /opt/software/bm10/jcrew1/appserver/weblogic/entapps/
mv ${EAR_FILE_4} /opt/software/bm10/jcrew1/appserver/weblogic/entapps/
