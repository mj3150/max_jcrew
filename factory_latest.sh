#!/bin/bash

EAR_FILE='factory.ear'

if [ EAR_FILE != 'factory.ear' ]
then
	wget -O ${EAR_FILE} "http://nyc-wdevtool-01:8080/jenkins/job/integration-steel-appc/lastSuccessfulBuild/artifact/factory/factory-entapp/target/factory-latest.ear"
mv ${EAR_FILE} /opt/software/bm10/jcrew1/appserver/weblogic/entapps/
fi
