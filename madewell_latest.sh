#!/bin/bash

EAR_FILE='madewell.ear' 

if [ EAR_FILE != 'madewell.ear' ] 
then 
	wget -O ${EAR_FILE} "http://nyc-wdevtool-01:8080/jenkins/job/integration-steel-appc/lastSuccessfulBuild/artifact/madewell/madewell-entapp/target/madewell-latest.ear"
mv ${EAR_FILE} /opt/software/bm10/jcrew1/appserver/weblogic/entapps/
fi

