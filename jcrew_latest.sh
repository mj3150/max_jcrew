#!/bin/bash

EAR_FILE='jcrew.ear'

if [ EAR_FILE != 'jcrew.ear' ]
then
	 wget -O ${EAR_FILE}  "http://nyc-wdevtool-01:8080/jenkins/job/integration-steel-appc/lastSuccessfulBuild/artifact/jcrew/jcrew-entapp/target/jcrew-latest.ear"
mv ${EAR_FILE} /opt/software/bm10/jcrew1/appserver/weblogic/entapps/
fi
